extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var strokes=[]
var renderedStrokes=[]
var graphicsObjects=[]
var renderedGraphicsObjects=[]
# Called when the node enters the scene tree for the first time.
var svgHeader1="<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"no\"?>\n<svg\n   xmlns:dc=\"http://purl.org/dc/elements/1.1/\"\n   xmlns:cc=\"http://creativecommons.org/ns#\"\n   xmlns:rdf=\"http://www.w3.org/1999/02/22-rdf-syntax-ns#\"\n   xmlns:svg=\"http://www.w3.org/2000/svg\"\n   xmlns=\"http://www.w3.org/2000/svg\"\n   viewBox=\"0 0 "
var svgHeader2="\"\n   version=\"1.1\"\n   id=\"svg2\"\n   height=\""
var svgHeader3="   \"\n   width=\""
var svgHeader4="   \">\n  <defs     id=\"defs4\" />\n  <metadata\n     id=\"metadata7\">\n    <rdf:RDF>\n      <cc:Work\n         rdf:about=\"\">\n        <dc:format>image/svg+xml</dc:format>\n        <dc:type\n           rdf:resource=\"http://purl.org/dc/dcmitype/StillImage\" />\n        <dc:title></dc:title>\n      </cc:Work>\n    </rdf:RDF>\n  </metadata>\n  <g\n     id=\"layer1\" >\n"
var svgSnipplet1="<path\n       id=\"path"
var svgSnipplet1_1="\"\n       d=\"M "
var svgSnipplet2=" L "
var svgSnipplet3="\"\nstyle=\"opacity:"
var svgSnipplet4=";fill:"
var svgSnipplet5=";fill-opacity:"
var svgSnipplet6=";stroke:"
var svgSnipplet7=";stroke-width:"
var svgSnipplet8=" ;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:4;stroke-dasharray:none;stroke-dashoffset:0;stroke-opacity:1\" />\n" 
var svgSnipplet9= " </g> </svg>\n"
var resolution=Vector2(1200, 720)

func _ready():
	resolution=get_viewport_rect().size

func genSVGString(strokes=strokes, graphicsObjects=graphicsObjects):
	var svgString=svgHeader1
	svgString=svgString+"%d"%resolution.x + " "+"%d"%resolution.y
	svgString=svgString+svgHeader2+"%d"%resolution.y
	svgString=svgString+svgHeader3+"%d"%resolution.x+svgHeader4
	
	for i in range(graphicsObjects.size()):
		svgString=svgString+svgSnipplet1+"%d"%i+svgSnipplet1_1
		if strokes[i].size()> 0:
			svgString=svgString+"%d"%strokes[i][0].x  +", "+"%d"%strokes[i][0].y 
			svgString=svgString+svgSnipplet2
			for j in range(1, strokes[i].size()):
				svgString=svgString+"%d"%strokes[i][j].x+", "+"%d"%strokes[i][j].y  +" "
			svgString=svgString+svgSnipplet3
			if graphicsObjects[i][0]==0:
				svgString=svgString+"%f"%graphicsObjects[i][5]+svgSnipplet4+"#"+graphicsObjects[i][4].to_html(false)
				svgString=svgString+svgSnipplet5+"%f"%(graphicsObjects[i][5]*2.0)
				svgString=svgString+svgSnipplet6+"none"+svgSnipplet7+"%d"%graphicsObjects[i][1]+svgSnipplet8
			else:
				svgString=svgString+"%f"%graphicsObjects[i][5]+svgSnipplet4+"none"
				svgString=svgString+svgSnipplet5+"%f"%(graphicsObjects[i][5]*2.0)
				svgString=svgString+svgSnipplet6+"#"+graphicsObjects[i][4].to_html(false)+svgSnipplet7+"%d"%graphicsObjects[i][1]+svgSnipplet8
	svgString=svgString+svgSnipplet9
	return svgString
	
func clearChildren():
	for child in get_children():
		remove_child(child)
		child.queue_free()

func getStrokeCount():
	var count=0
	for item in strokes:
		count=count+item.size()-1
	return count

