extends Line2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var fingers=1
var renderedLines=[]
var radius=1
var alpha=0.5
#[1, penSize, fingers, variance, colour, alpha]
var renderedGraphicObjects=[]
var renderedStrokes=[[]]
var variance=0.0
var utility=null
# Called when the node enters the scene tree for the first time.
func _ready():
	pass
func addPoints(newPoints):
	var newP=PackedVector2Array(newPoints)
	points.append_array(newP)
	renderedStrokes[0]=renderedStrokes[0]+newPoints
	if fingers>1:
		for i in range( fingers):
			var newRadius=radius
			var colour_var=default_color
			if i > 0:
				renderedStrokes.append([])
			if utility!=null:
				if variance >0:
					newRadius=utility.apply_variance_radius(variance, radius)
					var h_in = default_color.h
					var new_h = utility.apply_colour_variance(fingers, colour_var, i, variance)
					var col=Color.from_hsv(new_h*1.0/360.0, default_color.s, default_color.v)
					colour_var=col 
					default_color=col
			renderedGraphicObjects.append([1, newRadius, 1, variance, colour_var, alpha])
			for j in range(1,newP.size()-1):

				var strokeOrientation=newP[j-1]-newP[j]
				strokeOrientation=strokeOrientation.normalized()
				var normals=[Vector2(-strokeOrientation.y, strokeOrientation.x), Vector2(strokeOrientation.y, -strokeOrientation.x)]
				var renderedPoint=newP[j]+normals[i%2]*(newRadius*i)
				if i > 0:
					if j==1:
						#add first point
						var firstPoint=newP[0]+normals[i%2]*(newRadius*i)
						renderedStrokes[i].append(firstPoint)
					renderedStrokes[i].append(renderedPoint)
	#else:
		#renderedStrokes[0]=renderedStrokes[0]+newPoints
					
func manipulate_line(point):

	add_point(point)
	renderedStrokes[0].append(point)
	if fingers >1:
		if points.size()==1:
			for i in range( fingers):
				var line=Line2D.new()
				
				line.default_color=default_color
				line.begin_cap_mode=Line2D.LINE_CAP_ROUND
				line.end_cap_mode=Line2D.LINE_CAP_ROUND
				var colour_var=default_color
				var newRadius=radius
				if utility!=null:
					if variance >0:
						newRadius=utility.apply_variance_radius(variance, radius)
						var h_in = default_color.h
						var new_h = utility.apply_colour_variance(fingers, colour_var, i, variance)
						var col=Color.from_hsv(new_h*1.0/360.0, default_color.s, default_color.v, alpha)
						line.default_color=col
				if newRadius< 1.5:
					line.antialiased=true
				line.width=newRadius
				
				renderedGraphicObjects.append([1, newRadius, 1, variance, colour_var, alpha])
				renderedLines.append(line)
				if i > 0:
					renderedStrokes.append([])
					add_child(line)
		elif points.size()>1:
			var strokeOrientation=points[points.size()-2]-points[points.size()-1]
			strokeOrientation=strokeOrientation.normalized()
			var normals=[Vector2(-strokeOrientation.y, strokeOrientation.x), Vector2(strokeOrientation.y, -strokeOrientation.x)]
			for i in range( fingers):
				var renderedPoint=point+normals[i%2]*(radius*i)
				if points.size()==2:
					#add first point
					var firstPoint=points[0]+normals[i%2]*(radius*i)
					renderedLines[i].add_point(firstPoint)
				renderedLines[i].add_point(renderedPoint)
				if i > 0:
					renderedStrokes[i].append(renderedPoint)
	else:
		renderedGraphicObjects.append([1, radius, 1, variance, default_color, alpha])
				
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
