extends Polygon2D
var intersections=[]
var intersections2=[]
var polys=[]
var points=[]
var rendered=null
var colour=Color(0,0,0,0.5)
var renderedStrokes=[[]]
var intersections1Counter=0
var intersections2Counter=0

func _ready():
	pass # Replace with function body.

func isLeft(c,a=points[0], b=points[points.size()-1]):
	
	return ((b.x - a.x)*(c.y - a.y) - (b.y - a.y)*(c.x - a.x)) > 0;

func overlapRect(a,b,c,d):
	
	# If one rectangle is on left side of other 
	if a.x >= d.x or c.x >= b.x: 
		return false
  
	# If one rectangle is above other 
	if a.y <= d.y or c.y <= b.y: 
		return false
	
	return true

func same_points(a, b, c, d):
	if a==b or a==c or a==d:
		return true
	if b==c or b==d:
		return true
	if c==d:
		return true
	return false

func isclose(a, b):
	if abs(a-b) < 0.001:
		return true
	else:
		return false
		
func lineIntersect(a, b, c, d):
	# from https://gist.github.com/EgoMoose/f3d22a503e0598c12d1e3926dc72fb19
	var r = (b - a);
	var s = (d - c);
	var dd = r.x * s.y - r.y * s.x; 
	if dd==0:
		return null
	var u = ((c.x - a.x) * r.y - (c.y - a.y) * r.x) / dd;
	var t = ((c.x - a.x) * s.y - (c.y - a.y) * s.x) / dd;
	if((0 <= u and u <= 1 and 0 <= t and t <= 1) and a + t * r):
		var result=a+t*r
		
		return result
	else:
		return false
	#return (0 <= u and u <= 1 and 0 <= t and t <= 1) and a + t * r;


func intersectLines(a,b,c,d):
	var result=null
	
	#result=lineIntersect(a,b,c,d)
	result = Geometry2D.segment_intersects_segment(a, b, c, d)
	
	return result
		
		
func add_polygon_loops(intersection):
	var polyPoints= PackedVector2Array()
	#var line=Line2D.new()
	#line.width=2
	#line.default_color=Color(0.2,0.7,0.3)
	var newPoly=Polygon2D.new()
	var polyPoints2= PackedVector2Array()
	var end=intersection[1]
	var i = intersection[0]

		
	polyPoints2.append(intersection[2])
	#line.add_point(intersection[2])
	polyPoints2.append(points[i])
	#line.add_point(points[i])
	
	i=i+1
	
	while i < end:
		polyPoints2.append(points[i])
		#line.add_point(points[i])
		
			
		for k in intersections2:
			if i==k[0]:
				#end=k[1]
				polyPoints2.append(k[2])
				#line.add_point(k[2])
				i=k[1]
		i=i+1
	
	polyPoints2.append(intersection[2])
	#line.add_point(intersection[2])

	#add_child(line)
	newPoly.polygon=polyPoints2
	newPoly.color=colour
	polys.append(newPoly)
	
	add_child(newPoly)
	
	return 
func clearIntersections1():
	intersections.clear()
	intersections=[]
	
func clearIntersections2():
	intersections2.clear()
	intersections2=[]
	
	
func getIntersections1():
	var side=false
	var lastLoopPoint=points[0]
		
	for i in range(0,points.size()-1):
		intersections1Counter=i
		if(i>1):
			var newSide=isLeft(points[i])
			if newSide!=side:
				
				var item=[i,intersectLines(points[0], points[points.size()-1], points[i-1], points[i])]
				if item!=null:
					intersections.append(item)
					lastLoopPoint=points[i-1]
			side=newSide
		else:
			side=isLeft(points[i])
			
func getIntersections2():
	var mindistance=0.2
	for i in range(0, points.size()-1):
		intersections2Counter=i
		if(i>1):
			for j in range(i, points.size()-1):
				if(abs(i-j))>1 and points[i].distance_to(points[j])> mindistance:
					var intersectionPoint=intersectLines(points[i-1], points[i], points[j-1], points[j])
					if intersectionPoint:
						var item=[i,j, intersectionPoint]

						intersections2.append(item)


func add_polygon_noloops(startpoint):
	var endpoint=startpoint
	var polyPoints= PackedVector2Array()
	var i=startpoint
	var jumped=false
	var lastpoint=startpoint
	var cut=false
	#var line=Line2D.new()
	var lastIntersectionIndex=-1
	#line.width=2
	var rng = RandomNumberGenerator.new()
	rng.randomize()
	
	#line.default_color=Color(rng.randfn(), rng.randfn(), rng.randfn())
	var firstPoint=points[startpoint]
	for k in range(0, intersections.size()):
		if intersections[k][0]==startpoint-1:
			if intersections[k][1] and typeof(intersections[k][1])!= TYPE_BOOL:
				firstPoint=intersections[k][1]
				#polyPoints.append(points[0])
				polyPoints.append(intersections[k][1])
				#line.add_point(intersections[k][1])
	#for k in intersections2:
	#		if k[0]==startpoint-1 or k[1]==startpoint -1 :
				#polyPoints.append(points[0])
				#polyPoints.append(k[2])
	#			line.add_point(k[2])
				
	while i < points.size():
		var jump=false
		#polyPoints.append(points[i])
		var latestPoint=points[i]
		lastpoint=i
		for k in intersections2:
			if k[0]==i:
				jump=true
				jumped=true
				polyPoints.append(k[2])
				#line.add_point(k[2])
				i=k[1]

					
		for k in range(0, intersections.size()):
			if intersections[k][0]==i:
				if typeof(intersections[k][1])!= TYPE_BOOL:
					if intersections[k][1]!=null:
						polyPoints.append(intersections[k][1])
						#line.add_point(intersections[k][1])
						cut=true
						lastIntersectionIndex=k
				
		
		polyPoints.append(points[i])
		#line.add_point(points[i])
		if not jumped:
			endpoint=lastpoint
		if cut:
			break

		if not jump:
			i=i+1
			
	var newPoly=Polygon2D.new()
	
	newPoly.polygon=polyPoints
	newPoly.color=colour
	#var rng = RandomNumberGenerator.new()
	#rng.randomize()
	#newPoly.color=Color(rng.randfn(), rng.randfn(), rng.randfn())
	polys.append(newPoly)
	add_child(newPoly)
	#add_child(line)
	if cut:
		return endpoint 
	else:
		return points.size()-1
func add_polygon3(startpoint):
	var endpoint=startpoint
	var polyPoints= PackedVector2Array()
	var i=startpoint
	var jumped=false
	var lastpoint=startpoint
	var cut=false
	var line=Line2D.new()
	var lastIntersectionIndex=-1
	line.width=2
	
	
	while i < points.size():
		var jump=false
		#polyPoints.append(points[i])
		var latestPoint=points[i]
		lastpoint=i
		for k in intersections2:
			if k[0]==i and i!=startpoint:
				if jumped:
					print("jump:2")
					print(i)
					print(k[1])
					cut=true
				else:
					jump=true
					jumped=true
					polyPoints.append(k[2])
					line.add_point(k[2])
					if endpoint==startpoint:
						endpoint=i
					print("jump:")
					print(i)
					print(k[1])
					var newPoly=Polygon2D.new()
					var polyPoints2= PackedVector2Array()
					if not jumped:
						for p in range(i,k[1]):
							polyPoints2.append(points[p])
							line.add_point(points[p])
						newPoly.polygon=polyPoints2
						newPoly.color=colour
						polys.append(newPoly)
						add_child(newPoly)
					
					i=k[1]
					
		if not jumped:
			for k in range(0, intersections.size()-1):
				if intersections[k][0]==i:
					if typeof(intersections[k][1])!= TYPE_BOOL:
						polyPoints.append(intersections[k][1])
						line.add_point(intersections[k][1])
						cut=true
						lastIntersectionIndex=k
				
		
		if not jump and not cut:
			if lastIntersectionIndex>0 and typeof(intersections[lastIntersectionIndex -1][1])!= TYPE_BOOL:
				polyPoints.append(intersections[lastIntersectionIndex -1][1])
				line.add_point(intersections[lastIntersectionIndex -1][1])
			else:
				polyPoints.append(latestPoint)
				line.add_point(latestPoint)
			
		if not jumped:
			endpoint=lastpoint
		if cut:
			break

		if not jump:
			i=i+1
			
	#polyPoints.append(points[0])
	if lastIntersectionIndex>0 and typeof(intersections[lastIntersectionIndex -1][1])!= TYPE_BOOL:
		line.add_point(intersections[lastIntersectionIndex -1][1])
	else:
		line.add_point(points[0])
	#rendered=Geometry.convex_hull(polyPoints)
	var newPoly=Polygon2D.new()
	
	newPoly.polygon=polyPoints
	newPoly.color=colour
	#var rng = RandomNumberGenerator.new()
	#rng.randomize()
	#newPoly.color=Color(rng.randfn(), rng.randfn(), rng.randfn())
	polys.append(newPoly)
	add_child(newPoly)
	add_child(line)
	
	return endpoint
func add_polygon2(startpoint):
	var endpoint=startpoint
	var polyPoints= PackedVector2Array()
	var i=startpoint
	var jumped=false
	var lastpoint=startpoint
	var cut=false
	
	while i < points.size():
		var jump=false
		polyPoints.append(points[i])
		lastpoint=i
		for k in intersections2:
			if k[0]==i:
				jump=true
				jumped=true
				polyPoints.append(k[2])
				if endpoint==startpoint:
					endpoint=i+1
				i=k[1]
		for k in intersections:
			if k[0]==i:
				if typeof(k[1])!= TYPE_BOOL:
					polyPoints.append(k[1])
					cut=true
				
		
		if not jumped:
			endpoint=lastpoint
		if cut:
			break

		if not jump:
			i=i+1
			



	#rendered=Geometry.convex_hull(polyPoints)
	var newPoly=Polygon2D.new()
	
	newPoly.polygon=polyPoints
	newPoly.color=colour
	polys.append(newPoly)
	add_child(newPoly)
	
	return endpoint

func addPoints(newPoints):
	points=points+newPoints
	renderedStrokes[0]=renderedStrokes[0]+newPoints
	
func manipulate_shape(point):
	if points.size()==0 or ((points.size() < 50 ) and points[points.size()-1].distance_to( point) > 5.0) or (points[points.size()-1].distance_to( point) > 10.0):
		points.append(point)
		renderedStrokes[0].append(point)
		
		for child in get_children():
			remove_child(child)
			child.queue_free()
		
		clearIntersections1()
		clearIntersections2()
		
		polys.clear()

		getIntersections1()
				
				
		getIntersections2()
		

		for intersection in intersections2:
			add_polygon_loops(intersection)
			
		
		var i=0
		while i < points.size():
			i=add_polygon_noloops(i)
			i=i+1



func manipulate_shape2(point):
	
	if points.size()==0 or points[points.size()-1].distance_to( point) > 3.0:
		points.append(point)
		renderedStrokes[0].append(point)

		

		var polyz= Geometry2D.decompose_polygon_in_convex(points)
		
		
		for child in get_children():
			remove_child(child)
			child.queue_free()
		
		#clearIntersections1()
		#clearIntersections2()
		
		polys.clear()

		getIntersections1()
		for polyPoints in polyz:
			var newPoly=Polygon2D.new()
			
			newPoly.polygon=polyPoints
			newPoly.color=colour
			polys.append(newPoly)
			add_child(newPoly)

	#getIntersections1()
			
			
	#getIntersections2()
	

	#for intersection in intersections2:
	#	add_polygon_loops(intersection)
		
	
	#var i=0
	#while i < points.size():
	#	i=add_polygon_noloops(i)
	#	i=i+1
