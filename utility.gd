extends Node


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	#seed(1234)
	randomize()

func apply_variance_radius(variance, radius):
	var newRadius=radius
	var randVar=randi() %10
	if randVar>variance*10:
		return newRadius
	var randVar2=randi() %10
	newRadius=newRadius *((randVar2*4.0)/9.0);
	return newRadius
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
func get_harmony_analogue (h_in, index):
	if index%3==0:
		return absf((h_in+30)-360);
	if index%3==1:
		return absf((h_in+60)-360);
	if index%3==2:
		return absf((h_in+90)-360);
	
func get_harmony_triadic (h_in, index):
	if index%3==0:
		return h_in
	if index%3==1:
		return absf((h_in+120)-360)
	if index%3==2:
		return absf((h_in+240)-360)
		
func get_harmony_tedradic (h_in, index):
	if index%4==3:
		return absf((h_in+30)-360)
	if index%4==1:
		return absf((h_in+60)-360)
	if index%4==2:
		return absf((h_in+90)-360)

func  get_harmony_complement(h_in, index):
	return abs((h_in+180)-360)

func apply_colour_variance(fingers, color, index, variance):

	var random_variable =randi() % 10
	var in_h = color.h
	if variance == 0:
		return color.h
		
	if fingers< 4 || variance <=0.2 || random_variable <2:
		var random_variable2=randi() % 20
		in_h += abs(random_variable2-360);

	if fingers< 4 || variance <=0.4 || random_variable <4:
		in_h = get_harmony_analogue(in_h, index)

	elif fingers<7|| variance < 0.6|| random_variable >4 && random_variable<8:
		in_h = get_harmony_triadic(in_h, index);

	elif variance > 0.7 || random_variable > 8:
		in_h = get_harmony_complement(in_h, index);

	else: #if(variance > 0.7||fingers>6
		in_h=get_harmony_tedradic(in_h, index);
	if in_h == null:
		in_h = color.h
	print(in_h)
	return in_h*1.0
