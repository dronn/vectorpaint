extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"


# Called when the node enters the scene tree for the first time.
func _ready():
	pass

func _draw():
	var poly=get_parent().poly
	if poly !=null:
		for intersection in poly.intersections:
			#var coord=poly.points[intersection[0]]
			if typeof(intersection[1])!= TYPE_BOOL:
				var coord=intersection[1]
				draw_circle(coord,10, Color(1,0,0,0.3))
		for intersection in poly.intersections2:
			#var coord=poly.points[intersection[0]]
			if typeof(intersection[2])!= TYPE_BOOL:
				var coord=intersection[2]
				draw_circle(coord,10, Color(0,0,1,0.5))

	
func _process(delta):
	queue_redraw()
# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
