extends Sprite2D
var img
var flipY=false
# Called when the node enters the scene tree for the first time.
func _ready():
	pass
	
func clear():
	update(false, true)
	
func update(wait=false, clearUp=false):
	var viewPort=get_parent()
	var viewportTex = viewPort.get_texture()
	
	img = viewportTex.get_image()
	var imgDest = Image.create(img.get_size().x, img.get_size().y, false, img.get_format())
	imgDest.clear_mipmaps()
	
	if clearUp == true:
		img.fill(Color(1,1,1))
		imgDest.fill(Color(1,1,1))
		imgDest.blit_rect(img, Rect2(Vector2(0,0),img.get_size()), Vector2.ZERO)
	else:
		imgDest.blit_rect(img, Rect2(Vector2(0,0),img.get_size()), Vector2.ZERO)
	
	imgDest.flip_y()
	var tex = ImageTexture.create_from_image(imgDest)
	
	if wait== true:
		await RenderingServer.frame_post_draw
	texture = tex

