extends Node2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var rectordStroke=false
var stroke=[]
var loop=true
var line=null
var poly=null
var historyObjects=[]
var colour=Color(0,0,0,1)
var alpha=0.5
var penSize=2
var fingers=1
var variance=0.0
var saving=false
var loading=false
var rerenderHistory=false
var rerenderObject=0
var byteBuff=null
var presetResolution
var uiActive=false
var loadIteration=0
var loadFileIdx=0
var loadStrokeHeader=[]
var scaleFactor=1.0
# Called when the node enters the scene tree for the first time.

func _init():
	presetResolution=null

func _ready():
	RenderingServer.set_default_clear_color(Color(1,1,1))
	var vpContainer=get_node("SubViewportContainer")
	var res
	if presetResolution!=null:
		res = presetResolution
	else:
		res = Vector2(ProjectSettings.get_setting("display/window/size/viewport_width"), 
					ProjectSettings.get_setting("display/window/size/viewport_height"))

	vpContainer.offset_bottom=res.y-1
	vpContainer.offset_right=res.x
	get_tree().get_root().connect("size_changed",Callable(self,"resizeme"))

func resizeme():
	print("Resizing: ", get_viewport_rect().size)
	
func loadDia():
	if OS.has_feature('JavaScript'):
		loadBinary2("res://test2.data")
	else:
		var fileDialogue=get_node("ui/loadFiledialogue")
		fileDialogue.fileLoad()
		saving=true
		
func save():
	var historyNode=get_node("history")
	var saveStr=historyNode.genSVGString(historyNode.renderedStrokes, historyNode.renderedGraphicsObjects)
	if OS.has_feature('JavaScript'):
		saveJs("sketch.svg", saveStr)
	else:
		var fileDialogue=get_node("ui/filedialogue")
		fileDialogue.fileSave()
		saving=true
		
func saveFile(path="user://"):
	var filepath=path+"Vector.svg"
	var historyNode=get_node("history")
	var saveStr=historyNode.genSVGString(historyNode.renderedStrokes, historyNode.renderedGraphicsObjects)
	var file = FileAccess.open(filepath, FileAccess.WRITE)
	file.store_string(saveStr)
	file.close()
		
func saveJs(filename, text):
	var text2=text
	text2=text2.replace("\"", "\\\"")
	var js="var element = document.createElement('a');\n"
	js=js+"element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(\""+text2.replace("\n", "\\n")+"\"));\n"
	js=js+"element.setAttribute('download', \""+filename+"\");\n"
	js=js+"element.style.display = 'none';\n"
	js=js+"document.body.appendChild(element);\n"
	js=js+"element.click();\n"
	js=js+"document.body.removeChild(element);"
	print(js)
	if OS.has_feature('web'):
		JavaScriptBridge.eval(js)
	
func getTexture():
	var bufferNode=get_node("SubViewportContainer/SubViewport/Buffer")
	var viewPort=get_node("SubViewportContainer/SubViewport")
	var img = viewPort.get_texture().get_image()

	var imgDest = Image.create(img.get_size().x, img.get_size().y, false, img.get_format())
	imgDest.blit_rect(img, Rect2(Vector2(0,0),img.get_size()), Vector2.ZERO)

	if bufferNode.flipY:
		imgDest.flip_y()
	var tex = ImageTexture.create_from_image(imgDest)
	#await RenderingServer.frame_post_draw
	return tex

func saveBinary(filename):
	byteBuff=PackedByteArray()
	var idx=0
	var history=get_node("history")
	idx=bytesFromHistory(history,  byteBuff, idx)
	var file = FileAccess.open(filename+"_b", FileAccess.WRITE)
	print(byteBuff.size())
	print(byteBuff)
	for byte in byteBuff:
		file.store_8(byte)
	file.close()
	
func saveBinary2(filename, binary):
	var file = FileAccess.open(filename, FileAccess.WRITE)
	for byte in binary:
		file.store_8(byte)
	file.close()
	
func loadBinaryToArray(filename):
	var byteBuff_=PackedByteArray()
	var file = FileAccess.open(filename, FileAccess.READ)
	while(not file.eof_reached()):
		byteBuff_.append(file.get_8())

	return byteBuff_
	
func loadSerializedToArray(filename):
	var byteBuff_=PackedByteArray()
	var file =FileAccess.open(filename, FileAccess.READ)
	var filelen=file.get_length()
	for i in range(0,6):
		file.get_16()
		
	while(not file.eof_reached()):
		byteBuff_.append(file.get_8())

	file.close()
	return byteBuff_

func get16(byteBuff_, idx):
	var byte0=byteBuff_[idx*2]
	var byte1=byteBuff_[idx*2+1]
	var result16:int=byte1*256+byte0
	return result16
	
func bytesFromInt(integer:int, byteBuff2, idx):
	var byte1:int=integer/256
	var byte0:int=integer-byte1*256
	byteBuff.append(byte0)
	byteBuff.append(byte1)
	return idx+2
	
func headerBytesFromGraphicsObject(graphicsObject, byteBuff, idx):
	var loop:int=graphicsObject[0]-1
	if loop==-1:
		loop=1
	idx = bytesFromInt(loop, byteBuff, idx)
	idx = bytesFromInt(graphicsObject[1]*1.0*100.0, byteBuff, idx)
	idx = bytesFromInt(graphicsObject[2], byteBuff, idx)
	idx = bytesFromInt(graphicsObject[3]*1.0*100.0, byteBuff, idx)
	idx = bytesFromInt(graphicsObject[4].r*1.0*65535.0, byteBuff, idx)
	idx = bytesFromInt(graphicsObject[4].g*1.0*65535.0, byteBuff, idx)
	idx = bytesFromInt(graphicsObject[4].b*1.0*65535.0, byteBuff, idx)
	idx = bytesFromInt(graphicsObject[5]*1.0*100.0, byteBuff, idx)
	return idx
	
func bytesFromCoords(vector, byteBuff, idx):
	idx = bytesFromInt(vector.x, byteBuff, idx)
	idx = bytesFromInt(vector.y, byteBuff, idx)
	return idx
	
func bytesFromHistory(history, byteBuff2, idx):
	
	var header_seed =1227
	idx = bytesFromInt(header_seed,byteBuff, idx)
	var header_strokes=history.graphicsObjects.size()
	idx=bytesFromInt(header_strokes, byteBuff2, idx)
	var header_size=3
	var stroke_header_size=9
	var overall_coordinates=history.getStrokeCount()
	var header_filesize=header_size+ header_strokes*stroke_header_size+ overall_coordinates*2
	idx=bytesFromInt(header_filesize, byteBuff2, idx)
	var strokeIndex=0
	for go in history.graphicsObjects:
		idx=headerBytesFromGraphicsObject(go,byteBuff2,idx)
		idx=bytesFromInt(history.strokes[strokeIndex].size()-1, byteBuff2, idx)
		strokeIndex=strokeIndex+1
		
	for stroke in history.strokes:
		for i in range(0,stroke.size()-1):
			idx=bytesFromCoords(stroke[i], byteBuff2, idx)
	return idx

func loadBinary2(filename):
	byteBuff=loadBinaryToArray(filename)

	var idx=0
		#// header: 0 seed 1 number of strokes 2 overall size
	var header_seed = get16(byteBuff, idx)
	idx=idx+1
	var header_strokes= get16(byteBuff, idx)
	idx=idx+1
	var header_filesize=get16(byteBuff, idx)
	idx=idx+1

	for i in range(0, header_strokes):
		var headerItem=[]
		var strokeType= get16(byteBuff, idx)
		idx=idx+1
		headerItem.append(strokeType)
		var radius= get16(byteBuff, idx)
		idx=idx+1
		headerItem.append(radius)
		var fingers=get16(byteBuff, idx)
		idx=idx+1
		headerItem.append(fingers)
		var variance=get16(byteBuff, idx)
		idx=idx+1
		headerItem.append(variance)
		var red=get16(byteBuff, idx)
		idx=idx+1
		headerItem.append(red)
		var green=get16(byteBuff, idx)
		idx=idx+1
		headerItem.append(green)
		var blue=get16(byteBuff, idx)
		idx=idx+1
		headerItem.append(blue)
		var alpha=get16(byteBuff, idx)
		idx=idx+1
		headerItem.append(alpha)
		var numCoords=get16(byteBuff, idx)
		idx=idx+1
		headerItem.append(numCoords)
		loadStrokeHeader.append(headerItem)
		
	idx=loadStroke( loadStrokeHeader[0], byteBuff, idx)
	loadFileIdx=idx
	loadIteration=1
	rerenderHistory=true
	
func loadStroke( strokeHeader, byteBuff, idx):
	stroke=[]
	var coord_x=get16(byteBuff, idx)
	idx=idx+1
	var coord_y=get16(byteBuff, idx)
	idx=idx+1
		#	strokeCoords.append(Vector2(coord_x, coord_y))
	draw_coord(strokeHeader, Vector2(coord_x*scaleFactor, coord_y*scaleFactor))
	var points=[]
	var end=strokeHeader[8]-1
	for j in range(0, end):
		coord_x=get16(byteBuff, idx)
		idx=idx+1
		coord_y=get16(byteBuff, idx)
		idx=idx+1
		points.append(Vector2(coord_x*scaleFactor, coord_y*scaleFactor))

	if strokeHeader[0]==1:
		poly.addPoints(points)
	else:
		line.addPoints(points)
	stroke=stroke+points

	draw_coord(strokeHeader, Vector2(coord_x*scaleFactor, coord_y*scaleFactor))
	finishStroke(strokeHeader)
	return idx

func draw_coord(strokeHeader, vector):
	var radius
	var fingers
	var variance
	var red
	var green
	var blue
	var alpha
	var loop=false
	
	if(strokeHeader.size()>6):
		radius=strokeHeader[1]*1.0/100.0
		fingers=strokeHeader[2]
		variance=strokeHeader[3]*1.0/100.0
		red=strokeHeader[4]*1.0/65535.0
		green=strokeHeader[5]*1.0/65535.0
		blue=strokeHeader[6]*1.0/65535.0
		alpha=strokeHeader[7]*1.0/100.0
		if strokeHeader[0]==1:
			loop=true

	else:
		radius=strokeHeader[1]
		fingers=strokeHeader[2]
		variance=strokeHeader[3]
		red=strokeHeader[4].r
		green=strokeHeader[4].g
		blue=strokeHeader[4].b
		alpha=strokeHeader[5]
		if strokeHeader[0]==0:
			loop=true

	if(not loop):
		if stroke.size()==0:
			var col=Color(red, green, blue, alpha)
			stroke.append(vector)
			line=get_node("SubViewportContainer/SubViewport/line").duplicate()
			var utility=get_node("utility")
			line.utility=utility
			var new_h = utility.apply_colour_variance(fingers, col, 0, variance)
			var col2 = Color.from_hsv(new_h*1.0/360.0, col.s, col.v, alpha)
			line.default_color=col2
			line.width=radius
			line.radius=radius
			line.alpha=alpha
			line.fingers=fingers
			line.variance=variance
			line.begin_cap_mode=Line2D.LINE_CAP_ROUND
			line.end_cap_mode=Line2D.LINE_CAP_ROUND
			line.antialiased=true
			line.manipulate_line(vector)
			var viewport=get_node("SubViewportContainer/SubViewport")
			viewport.add_child(line)

		else:
			line.manipulate_line(vector)
			stroke.append(vector)
	else:
		if stroke.size()==0:
			stroke.append(vector)
			poly=get_node("SubViewportContainer/SubViewport/poly").duplicate()
			poly.colour=Color(red, green, blue, alpha)
			poly.manipulate_shape(vector)
			var viewport=get_node("SubViewportContainer/SubViewport")
			viewport.add_child(poly)

		else:
			stroke.append(vector)
			poly.manipulate_shape(vector)


func finishStroke(strokeHeader):
	var history=get_node("history")

	var radius
	var fingers
	var variance
	var red
	var green
	var blue
	var alpha

	radius=strokeHeader[1]*1.0/100.0
	fingers=strokeHeader[2]
	variance=strokeHeader[3]*1.0/100.0
	red=strokeHeader[4]*1.0/65535.0
	green=strokeHeader[5]*1.0/65535.0
	blue=strokeHeader[6]*1.0/65535.0
	alpha=strokeHeader[7]*1.0/100.0
	var viewport=get_node("SubViewportContainer/SubViewport")
	
	if(strokeHeader[0]==0):
		if line!=null:
			line.visible=false
			history.strokes.append(stroke)
			viewport.remove_child(line)
			history.add_child(line)
			for i in range(line.fingers):
				history.renderedGraphicsObjects.append(line.renderedGraphicObjects[i])
				line.alpha=alpha
				history.renderedStrokes.append(line.renderedStrokes[i])
			var newGraphicsObject=[1, radius, fingers, variance, Color(red, green, blue, alpha), alpha]
			history.graphicsObjects.append(newGraphicsObject)
	else:
		if poly!=null:
			poly.visible=false
			history.strokes.append(stroke)
			viewport.remove_child(poly)
			history.add_child(poly)
			var newGraphicsObject=[0, radius, fingers, variance, Color(red, green, blue,alpha), alpha]
			history.graphicsObjects.append(newGraphicsObject)
			history.renderedGraphicsObjects.append(newGraphicsObject)
			history.renderedStrokes.append(poly.renderedStrokes[0])

	poly=null
	line=null
	stroke=[]

func process_redraw():
	
	if rerenderHistory and loadIteration < loadStrokeHeader.size():
		
		loadFileIdx = loadStroke( loadStrokeHeader[loadIteration], byteBuff, loadFileIdx)
		loadIteration = loadIteration+1
	if rerenderHistory:
		var history=get_node("history")
		var buffer=get_node("SubViewportContainer/SubViewport/Buffer")
		buffer.update()
		queue_redraw()
		await get_tree().process_frame
		stroke=[]
		var viewport = get_node("SubViewportContainer/SubViewport")
		if(rerenderObject >= history.renderedGraphicsObjects.size()-2 ):
			if loadIteration >= loadStrokeHeader.size():
				rerenderHistory = false
				rectordStroke=false
				loading = false
				history.clearChildren()
				for object in historyObjects:
					viewport.remove_child(object)
					object.queue_free()
			for object in historyObjects:
				object.visible=false
			line=null
			poly=null
			stroke=[]
		else:
			while rerenderObject < history.renderedGraphicsObjects.size():
				for coord in history.renderedStrokes[rerenderObject]:
					draw_coord(history.renderedGraphicsObjects[rerenderObject], coord)
					if history.renderedGraphicsObjects[rerenderObject][0]==0:
						historyObjects.append(poly)
					else:
						historyObjects.append(line)
				rerenderObject=rerenderObject+1
				stroke=[]



func savePng(filename):
	var tex= await getTexture()
	if tex!=null:
		var img=tex.get_data()
		img.flip_y()
		img.clear_mipmaps()
		var x = img.save_png(filename)
		return true
	else:
		return false
