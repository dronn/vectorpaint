extends MeshInstance2D


# Declare member variables here. Examples:
# var a = 2
# var b = "text"
var points=[]
var loopMaterial= preload("res://loopMaterial.tres")

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func isLeft(c,a=points[0], b=points[points.size()-1]):
	
	return ((b.x - a.x)*(c.y - a.y) - (b.y - a.y)*(c.x - a.x)) > 0;

func manipulate_shape(point):
	points.append(point)
	var lastLoopPoint=points[0]
	var surface_tool = SurfaceTool.new();
	var side=false
	var intersections=[]
	
	for i in range(0,points.size()-1):
		if(i>1):
			var newSide=isLeft(points[i])
			if newSide!=side:
				intersections.append(i)
				lastLoopPoint=points[i-1]
			side=newSide
		else:
			side=isLeft(points[i])
	
	print(intersections)
	surface_tool.begin(Mesh.PRIMITIVE_TRIANGLES);
	for i in range(0,points.size()-1):
		surface_tool.add_vertex(Vector3(points[i].x, points[i].y, 0))
		
		if(i>1):
			var newSide=isLeft(points[i])
			if newSide!=side:
				intersections.append(i)

			side=newSide
		else:
			side=isLeft(points[i])
		if(i>2):
			surface_tool.add_vertex(Vector3(points[i-1].x, points[i-1].y, 0))
			surface_tool.add_vertex(Vector3(lastLoopPoint.x, lastLoopPoint.y, 0))
	var material=loopMaterial
	surface_tool.set_material(material)
	mesh=surface_tool.commit()
	#mesh.material_override.albedo_color(Color(0,0,0,0.5))
	

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass
